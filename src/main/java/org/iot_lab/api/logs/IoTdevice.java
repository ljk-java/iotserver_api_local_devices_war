package org.iot_lab.api.logs;

/**
 * Simple clasification of IoT device name versus id
 * @author leo
 */
public class IoTdevice
{
   int id;
   String name;

    public IoTdevice(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }
   
   
    
    
}

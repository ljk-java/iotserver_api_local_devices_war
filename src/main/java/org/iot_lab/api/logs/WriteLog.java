package org.iot_lab.api.logs;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*; // Date
import java.text.*; // DateFormat

/*
 * Servlet to write data to logfile
 * data is written to file with DeviceID.year-month-day.log
 * devices can log multiple data in logfile
 * When no DeviceID is given an error is thrown
 *
 * server02 API
 * http://192.168.65.48/readlog?DeviceID=NET01&Tout
 * http://192.168.65.48/writelog?DeviceID=TEST&Tout=20.3
 * http://192.168.65.48/writelog?DeviceID=TEST&SensorID=out&temperature=20.3
 * http://192.168.65.48/writelog?DeviceID=TEST&SensorID=out&temperature=20.3&debug
 *
 * 2022-04-2022 changed data field and doGet method to local values, not global class values
 * closes PrintWrite out after doGet() request
 *
 */

public class WriteLog extends HttpServlet
{

    // root map of datalog
    private static final String LOGDIRECTORY = "/var/log/datalog/";
    
    // private LedPanel ledpanel;
    private IoTMonitorPanel iotMonitorPanel;
    
    // constructor
    public WriteLog()
    {
        // read once at startup!
       // ledpanel = new LedPanel();
       iotMonitorPanel = new IoTMonitorPanel();
    }

    // method called at every http get request
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
    	PrintWriter out;
        
    	String dateLog = "";
    	String dateLogFull = "";
    	
    	String remoteIPAddr = "";
    	String deviceName = "";
    	String valueName = "";
    	String valueData = "";
    	
    	// get date and format 
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
        dateLogFull = ft.format(dNow);
        ft = new SimpleDateFormat("yyyy-MM-dd");
        dateLog = ft.format(dNow);

        // get remote IP address
        remoteIPAddr = request.getRemoteAddr();

        // Set response content type
        response.setContentType("text/html");
        out = response.getWriter();
        
        // get parameters, first parameter should be DeviceID, second is unknown, but parameter with value
        Enumeration<String> paramNames = request.getParameterNames();

        // parameter counter to check number of parameters
        int paramCounter = 0;

        while (paramNames.hasMoreElements())
        {
            if (paramCounter == 0)
            {
                // DeviceID
                String paramName = (String) paramNames.nextElement();
                if (paramName.equals("DeviceID"))
                {
                    deviceName = request.getParameterValues(paramName)[0];
                } else
                {
                    deviceName = "error";
                }
            }

            if (paramCounter == 1)
            {
                String paramName = (String) paramNames.nextElement();
                // value
                valueName = paramName;
                valueData = request.getParameterValues(valueName)[0];
            }

            if (paramCounter == 2)
            {
                String paramName = (String) paramNames.nextElement();
                if (paramName.equals("debug"))
                {

                    // with any extra parameter debug
                    String title = "Using GET method to write Log Data";
                    String docType = "<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n";
                    out.println(docType
                            + "<html>\n"
                            + "<head><title>" + title + "</title></head>\n"
                            + "<body bgcolor=\"#f0f0f0\">\n"
                            + "<h1 align=\"center\">" + title + "</h1>\n"
                            + "<ul>\n"
                            + "  <li><b>DeviceID</b>: "
                            + deviceName + "\n"
                            + "  <li><b>valueName</b>: "
                            + valueName + "\n"
                            + "  <li><b>valueData</b>: "
                            + valueData + "\n"
                            + "  <li><b>dateLog</b>: "
                            + dateLog + "\n"
                            + "  <li><b>dateSample</b>: "
                            + dateLogFull + "\n"
                            + "  <li><b>remoteIPAddr</b>: "
                            + remoteIPAddr + "\n"
                            + "</ul>\n"
                            + "</body></html>");
                }
            }

            paramCounter++;
        }
        writeLog(dateLog, dateLogFull, remoteIPAddr, deviceName, valueName, valueData, out);
        out.close();
        // write to 
        writeApi(deviceName, valueName, valueData);
        
    }

    // method to write to logfile
    public void writeLog(String dateLog, String dateLogFull, String remoteIPAddr, String deviceName, String valueName, String valueData, PrintWriter out)
    {
        // writing data to file!
        String logFile = LOGDIRECTORY + deviceName + "." + dateLog + ".log";
        try
        {
        	// creat a file to change file rights attributes
        	File file = new File(logFile);
        	
            FileWriter fw = new FileWriter(file, true); //the true will append the new data
            
            // appends this string to the file

            fw.write(dateLogFull + " " + deviceName + " " + remoteIPAddr + " " + valueName + " " + valueData + "\n");
                
            // setting permissions to the file -> read only, not only user 
            file.setReadable(true, false);
            
            fw.close();
            
            out.println("OK$WriteLog%Logfile-Write");
            // write to panel
            // ledpanel.sendAlive(deviceName);
            iotMonitorPanel.sendAlive(deviceName);
            
        } catch (IOException ioe)
        {
            out.println("ERR$WriteLog%Logfile-Write");
            out.println(ioe.getMessage());
            System.err.println("IOException: " + ioe.getMessage());
        }
       
    }
    
    public void writeApi(String deviceName, String valueName, String valueData)
    {
    	// send http://web01.home.ned:8080/observations/writevalue?DeviceId=deviceName&valueName=valueData"
    	String sendURL = String.format("http://web01.home.ned:8080/api/v3/observations/writevalue?DeviceID=%s&%s=%s", deviceName, valueName, valueData);
    	try
		{
			sendGet(sendURL);
		} 
    	catch (Exception e)
		{
			System.out.println("Error sending to observations legacy API");
			e.printStackTrace();
		}
    	
    }

    private void sendGet(String urlString) throws Exception
    {
  
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        // this must be done to send the actual request
        int responseCode = con.getResponseCode();
        
        if (responseCode != 200)
        {
        	System.out.println(urlString + " Response Code : " + responseCode);
        }
        con.disconnect();
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.iot_lab.api.logs;

import java.util.List;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 *
 * @author leo
 */
public class LedPanel
{
   // define Devices vs Lednr
    IoTdeviceFile devFile;
    List<IoTdevice> devList;
    
    String socket = "http://esp3201";
    int lednr;
    
    static String devName = "";
    
    // count number of errors
    static int errorCount = 0;
    
    // constructor
    public LedPanel()
    {
        devFile = new IoTdeviceFile();
        devList = devFile.getDeviceList();      
    }        
    
    /**
     * sendAlive send devide id to the LED panel, so this can display the status
     * @param deviceID 
     */
    public void sendAlive(String deviceID)
    {
        
        // check if the name is the same, to prevent sending to much data to LedPanel
        
        if (!deviceID.contentEquals(devName))
        {
            devName = deviceID;
            // if deviceID not found, the lednumber is 0
            lednr = 0;
            // get lednr from devicelist
            devList.forEach( device ->
            {
                if (device.getName().contains(deviceID))
                {
                    lednr = device.getId();
                }
            });

            if (errorCount < 20)
            {    
                try
                {
                    // example: http://esp3201/led/0/set
                    sendGetv2(socket + "/led/" + lednr + "/set");
                    // recover
                    if (errorCount > 0)
                    {
                        errorCount--;
                    }

                } catch (Exception ex)
                {
                    errorCount++;
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
                    LocalDateTime now = LocalDateTime.now();  
                    System.out.println(dtf.format(now));  
                    System.out.print(" DeviceID: " + deviceID);
                    System.out.print(" http://" + socket + "/led/" + lednr + "/set");
                    System.out.println(" Error #" + errorCount +" sending GET-request");
                }
            }
            devName = deviceID;
        }
    }
    

    private void sendGetv2(String urlString) throws Exception
    {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        // int responseCode = con.getResponseCode();
        // System.out.println(devName + " Response Code : " + responseCode);
        con.disconnect();
    }
    
    
    
}

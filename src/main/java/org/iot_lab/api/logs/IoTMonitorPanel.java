/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.iot_lab.api.logs;

import java.util.List;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 * Interface for IoTMonitor
 * remember that the sendAlive is called several times due to number of sensors
 * one device. Hence the name check.
 * @author leo
 */
public class IoTMonitorPanel
{
   // define Devices vs id
    IoTdeviceFile devFile;
    List<IoTdevice> devList;
    
    // socket to connect
    String socket = "http://192.168.65.113:8000";
    
    // count number of errors
    static int errorCount = 0;
    
    String previousDeviceName = "xxx";
    int id = 0;
    
    // constructor
    public IoTMonitorPanel()
    {
        devFile = new IoTdeviceFile();
        devList = devFile.getDeviceList();      
    }        
    
    /**
     * sendAlive send devide id to the LED panel, so this can display the status
     * @param deviceName
     */
    public void sendAlive(String deviceName)
    {
        if(!deviceName.contentEquals(previousDeviceName))
        {
            id = 0;
        
            // get id from devicelist
            devList.forEach(device ->
            {
                if (device.getName().contains(deviceName))
                {
                    id = device.getId();
                }
            });

            if (errorCount < 20)
            {    
                try
                {
                    if (id > 0)
                    {   
                        sendGet(socket + "/reset/" + id + "");
                    }
                    // recover
                    if (errorCount > 0)
                    {
                        errorCount--;
                    }

                } catch (Exception ex)
                {
                    errorCount++;
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
                    LocalDateTime now = LocalDateTime.now();  
                    System.out.println(dtf.format(now));  
                    System.out.print(" DeviceName: " + deviceName + " ");
                    System.out.print(socket + "/reset/" + id);
                    System.out.println(" Error #" + errorCount +" sending GET-request");
                }
            }
            previousDeviceName = deviceName;
        }
    }
    
    private void sendGet(String urlString) throws Exception
    {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        // this must be done to send the actual request
        int responseCode = con.getResponseCode();
        if (responseCode != 200)
        {
        	System.out.println(urlString + " Response Code : " + responseCode);
        }
        con.disconnect();
    }
    
}

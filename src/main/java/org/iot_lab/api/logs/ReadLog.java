package org.iot_lab.api.logs;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*; // Date
import java.text.*; // DateFormat

/*
 * Servlet to read latest data sample from logfile
 * data is read from file with DeviceID.year-month-day.log
 * When no DeviceID is given an error is thrown
 *
 * server02 API examples:
 * http://192.168.65.48/readlog?DeviceID=TTN02&temperature
 * http://192.168.65.48/readlog?DeviceID=NET01&Tout
 * http://192.168.65.48/readlog?DeviceID=NET01&Tout&debug
 * http://192.168.65.48/readlog?DeviceID=NET01&Tout&json
 *
 *
 * 2022-04-30 changed to local variables in method doGet and closed out stream.
 *
 */

public class ReadLog extends HttpServlet
{

    // root map of datalog
    private static final String LOGDIRECTORY = "/var/log/datalog/";

    // method called
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
    	// local variables due to seperation of every request
    	String deviceID = "";
        String valueName = "";
        String dateLog = "";
   
        PrintWriter out;
        
        boolean debug = false;
        boolean json = false;
        
        // reset values due to session memory
        debug = false;
        json = false;

        // get date and format 
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        dateLog = ft.format(dNow);

        // get remote IP address
        // String remoteIPAddr = request.getRemoteAddr();

        // Set response content type
        response.setContentType("text/html");
        out = response.getWriter();

        // get parameters, first parameter should be DeviceID, second is unknown, but parameter with value
        Enumeration<String> paramNames = request.getParameterNames();

        // parameter counter to check number of parameters
        int paramCounter = 0;
        
       

        while (paramNames.hasMoreElements())
        {
            if (paramCounter == 0)
            {
                // DeviceID
                String paramName = (String) paramNames.nextElement();
                if (paramName.equals("DeviceID"))
                {
                    deviceID = request.getParameterValues(paramName)[0];
                } else
                {
                    out.println("ERR$ReadLog%no-DeviceID");
                }
            }

            if (paramCounter == 1)
            {
                // value                                        
                valueName = (String) paramNames.nextElement();
                // valueData = request.getParameterValues(valueName)[0];
            }

            if (paramCounter == 2)
            {
                String paramName = (String) paramNames.nextElement();
                if (paramName.equals("debug"))
                {
                    debug = true;
                }
                if (paramName.equals("json"))
                {
                    json = true;
                }
            }

            paramCounter++;
        }

        if (!deviceID.equals("") && !valueName.equals(""))
        {

            // check file existance, otherwise the day may be turned over (23.59).
            // set date 1 day back and try again.
            if (!logfileExists(deviceID, dateLog))
            {
                // one day back...
                Calendar cal = Calendar.getInstance();
                try
                {
                    cal.setTime(ft.parse(dateLog));
                } catch (Exception parseex)
                {
                    // do nothing
                }
                cal.add(Calendar.DATE, -1);  // number of days to add
                dateLog = ft.format(cal.getTime());
            }

            readLog(deviceID, valueName, dateLog, out, debug, json);
            out.close();
        }
    }

    // method to check logfile exists
    private boolean logfileExists(String deviceID, String dateLog)
    {
        String logFile = LOGDIRECTORY + deviceID + "." + dateLog + ".log";
        File file = new File(logFile);
        return file.exists();
    }

    // method to read from logfile
    private void readLog(String deviceID, String valueName, String dateLog, PrintWriter out, boolean debug, boolean json)
    {
    	String valueData = "";
    	
        // reading data from file!
        String logFile = LOGDIRECTORY + deviceID + "." + dateLog + ".log";

        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(logFile));
            String line;
            String foundLine = "empty";
            while ((line = reader.readLine()) != null)
            {
                // check valueName on this line
                String[] split = line.split("\\s+");
                if (split[5].equals(valueName))
                {
                    valueData = split[6];
                    foundLine = line;
                }
            }

            if (!(valueData.equals("")))
            {

                if (!json)
                {
                    if (debug)
                    {
                        out.println(foundLine);
                        out.println("<br>");
                        out.println(valueName);
                        out.println("<br>");
                    }
                    out.println(valueData);
                } else
                {
                    // json format requested
                	// not yet correct format, just for testing
                    out.println("{ \"temperature\": " + valueData + ",\"humidity\": " + 60 + " }");
                }

            } else
            {
                out.println("ERR$ReadLog%no-valueName-found");
            }
            reader.close();
        } catch (IOException ioe)
        {
            out.println("ERR$ReadLog%Logfile-Read");
            out.println(ioe.getMessage());
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

}

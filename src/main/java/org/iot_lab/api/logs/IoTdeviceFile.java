package org.iot_lab.api.logs;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Access to IoTdevicefile that describes the IoT devices and properties
 * @author leo
 */
public class IoTdeviceFile
{
	// config file with information about IoT devices
    private static final String DEVICEFILE = "/var/log/datalog/devices-new.json";
  
    public List<IoTdevice> getDeviceList()
    {
        List<IoTdevice> deviceList = null;
        try
        {
            Path filePath = Paths.get(DEVICEFILE);
            File file = filePath.toFile();
            
            deviceList = new ArrayList<IoTdevice>();
            
            if (!file.exists())
            {
                IoTdevice device = new IoTdevice(0, "ERROR reading file");    
                deviceList.add(device);
            } 
            else
            {
            	// read all data into string
            	String fileData = new String(Files.readAllBytes(filePath));
            	
                // put it in an jsonArray
                JSONArray jsonArray = new JSONArray(fileData);
		
                // System.out.println(jsonArray.toString());
                
                // at this point the file is read..

                //reading arrays from json, now the file itself is an array
                //JsonArray jsonArray = jsonObject.getJsonArray("devices");
               
                for(Object obj : jsonArray)
                {
                    JSONObject innerJsonObject = (JSONObject) obj;
                    
                    IoTdevice device = new IoTdevice(
                            innerJsonObject.getInt("id"),
                            innerJsonObject.getString("name")
                    ); 
                    
                    // System.out.println(innerJsonObject.getInt("id"));
                    // System.out.println(innerJsonObject.getString("name"));
                    
                    deviceList.add(device);
                }
                
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return deviceList;
    }
}
